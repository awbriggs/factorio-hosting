import pickle
from typing import Dict, List

from factoriohost.data import game_data
from factoriohost.requests.request_objects import CreateRequest

__STORAGE: Dict[str, game_data.GameData] = dict()


def get_game(game_id: str) -> game_data:
    if __STORAGE.get(game_id, None) is None:
        return None

    return __STORAGE[game_id]


def remove_game(game_id: str) -> None:
    if __STORAGE.get(game_id, None) is None:
        return

    __STORAGE.pop(game_id)


def create_game(container_id: str, game_id: str, request: CreateRequest) -> None:
    __STORAGE[game_id] = game_data.GameData(game_id, container_id, request.name, request.version)


def get_instance_names() -> List[str]:
    return [__STORAGE[x].game_name for x in __STORAGE.keys()]


def get_current_games() -> List[game_data.GameData]:
    return sorted(__STORAGE.values(), key=lambda data: data.game_name)


def update_game(game: game_data.GameData) -> None:
    if __STORAGE.get(game.gameid, None) is not None:
        __STORAGE.update({game.gameid: game})


def dump_storage(file_name: str) -> None:
    with open(file_name, 'wb') as fh:
        pickle.dump(__STORAGE, fh, pickle.HIGHEST_PROTOCOL)


def load_storage(file_name: str) -> None:
    new_data: Dict[str, game_data.GameData] = {}

    try:
        with open(file_name, 'rb') as fh:
            new_data.update(pickle.load(fh))
    except FileNotFoundError:
        pass

    __STORAGE.update(new_data)
