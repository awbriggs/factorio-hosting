from dataclasses import dataclass


@dataclass
class GameData:
    gameid: str = ""
    containerid: str = ""
    game_name: str = ""
    version: str = ""
    status: str = "Unknown"


