import os
import re
import shutil
from typing import List
from uuid import uuid4

import docker
from docker.errors import APIError, ContainerError, ImageNotFound, NotFound
from flask import current_app

from factoriohost.data import persistence
from factoriohost.errors.game_creation_error import GameCreationError
from factoriohost.data.game_data import GameData
from factoriohost.requests.request_objects import CreateRequest

__FACTORIO_IMAGE_NAME = 'dtandersen/factorio'


def refresh_internal_game_status():
    client = docker.from_env()
    game_data = persistence.get_current_games()

    for game in game_data:
        try:
            game.status = client.containers.get(game.containerid).status
        except NotFound as error:
            game.status = "Unknown"
            current_app.logger.exception("Container not found, but persisted", error)
        except APIError as error:
            current_app.logger.exception("Unknown error was encountered", error)
        finally:
            persistence.update_game(game)


def refresh_status(func):
    def wrapper(*args, **kwargs):
        refresh_internal_game_status()
        return func(*args, **kwargs)
    return wrapper


@refresh_status
def create_game(request: CreateRequest) -> None:
    client = docker.from_env()
    try:
        internal_id = str(uuid4())
        container = client.containers.create(__FACTORIO_IMAGE_NAME,
                                             version=request.version,
                                             detach=True,
                                             ports={'34197/udp': None, '27015/tcp': None},
                                             volumes={
                                                 setup_storage(internal_id, request.save_file): {'bind': '/factorio',
                                                                                                 'mode': 'rw'}},
                                             restart_policy={"Name": "always"})
        persistence.create_game(container.id, internal_id, request)
        container.start()
    except ContainerError as error:
        current_app.logger.exception("Container error", error)
        raise GameCreationError()
    except APIError as error:
        current_app.logger.exception("Docker API error", error)
        raise GameCreationError()
    except ImageNotFound as error:
        current_app.logger.exception("Docker image not found", error)
        raise GameCreationError()


@refresh_status
def start_game(game_id: str) -> bool:
    container_id = persistence.get_game(game_id)
    if container_id is None:
        return False
    client = docker.from_env()

    try:
        client.containers.get(container_id).start()
        return True
    except ContainerError as error:
        current_app.logger.exception("Container error", error)
    except APIError as error:
        current_app.logger.exception("Docker API error", error)

    return False


@refresh_status
def is_valid_version(version: str) -> bool:
    match = re.match(r'\d{1,2}.\d{1,2}.\d{1,2}|stable|latest', version)
    return match is None


@refresh_status
def setup_storage(game_id: str, save_file: str) -> str:
    storage_location = "{}/{}/factorio".format(current_app.config['STORAGE_LOCATION'], game_id)
    os.makedirs(storage_location)

    if save_file is not "":
        container_save_locations = storage_location + "/saves"
        os.makedirs(container_save_locations)
        shutil.copy(save_file, container_save_locations)

    return storage_location


@refresh_status
def delete_game(game_id: str) -> bool:
    container_id = persistence.get_game(game_id)
    if container_id is None:
        return False
    client = docker.from_env()

    try:
        client.containers.get(container_id).stop()
        client.containers.get(container_id).remove()
        persistence.remove_game(game_id)
        return True
    except ContainerError as error:
        current_app.logger.exception("Container error", error)
    except APIError as error:
        current_app.logger.exception("API Error", error)

    return False


@refresh_status
def get_current_games() -> List[GameData]:
    return persistence.get_current_games()
