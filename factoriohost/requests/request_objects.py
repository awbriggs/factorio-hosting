from dataclasses import dataclass


@dataclass
class CreateRequest:
    name: str
    version: str = "stable"
    save_file: str = ""
