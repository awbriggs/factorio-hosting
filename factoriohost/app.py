import os
import atexit

from flask import Flask, jsonify, make_response, render_template, request, flash
from werkzeug.utils import secure_filename

from factoriohost.data import persistence
from factoriohost.errors.game_creation_error import GameCreationError
from factoriohost.handler.game_handler import create_game, start_game, delete_game, get_current_games
from factoriohost.requests.request_objects import CreateRequest

app = Flask(__name__)
app.config.from_json('../config-dev.json')
ALLOWED_EXTENSIONS = {'zip'}


@app.before_first_request
def setup_persistence():
    if app.config.get('PERSISTENCE_DUMP_FILE', None) is not None:
        persistence.load_storage(app.config['PERSISTENCE_DUMP_FILE'])
        atexit.register(persistence.dump_storage, app.config['PERSISTENCE_DUMP_FILE'])


@app.route('/')
def entry():
    return render_template('index.html')


@app.route('/create', methods=['POST', 'GET'])
def create():
    if request.method == 'POST':
        form_request = CreateRequest(**request.form)
        try:
            if 'save' in request.files and request.files['save'] is not '':
                file = request.files['save']
                if allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
                    file.save(filepath)
                    form_request.save_file = filepath
            create_game(form_request)
            flash(u'Successfully created game', 'success')
        except GameCreationError:
            flash(u'Unknown error creating this', 'failure')
        return render_template('create.html')
    return render_template('create.html')


@app.route('/games', methods=['GET'])
def get_existing_games():
    return render_template('games.html', games=get_current_games())


@app.route('/games/settings/<string:game_id>', methods=['GET'])
def get_settings_for_game(game_id: str):
    return render_template('settings.html', game=game_id)


def allowed_file(filename: str) -> bool:
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run()
